#![feature(pattern)]

extern crate rfmod;

#[macro_use] extern crate fmod_utils;

fn main() {
  println!("fmod-utils example main...");

  // new sys
  let rfmod_sys = rfmod::Sys::new().unwrap();
  // print sys infos
  // note: printing system information before initialization will show output
  // device capabilities, but certain state will be zero initialized
  //fmod_utils::write_info_rfmod_sys (&mut std::io::stdout(), &rfmod_sys).unwrap();

  // init sys with parameters
  let max_channels = 16; // TODO: are more channels needed ?
  rfmod_ok!(rfmod_sys.init_with_parameters (
    max_channels,
    rfmod::InitFlag (rfmod::INIT_3D_RIGHTHANDED)));
  println!("...rfmod sys initialized...");
  // set reverb properties
  let mut reverb_properties = rfmod::ReverbProperties::default();
  reverb_properties.environment = 0;  // must be set to 0-25 to enable
  reverb_properties.decay_time = 8.0;
  rfmod_ok!(
    rfmod_sys.set_reverb_properties (reverb_properties));
  println!("...rfmod sys reverb properties set...");
  // setup listener
  let listener_position = rfmod::Vector { x: 0.0, y: 0.0, z: 2.0 };
  let listener_velocity = rfmod::Vector { x: 0.0, y: 0.0, z: 0.0 };
  let forward           = rfmod::Vector { x: 0.0, y: 0.0, z: -1.0 };
  let up                = rfmod::Vector { x: 0.0, y: 1.0, z: 0.0 };
  rfmod_ok!(rfmod_sys.set_3D_listener_attributes (
    0, &listener_position, &listener_velocity, &forward, &up));
  println!("...rfmod sys 3d listener attributes set...");

  // print sys infos
  fmod_utils::write_info_rfmod_sys (&mut std::io::stdout(), &rfmod_sys).unwrap();

  // new wav sound
  let mut default_soundex_info  = rfmod::CreateSoundexInfo::default();
  let rfmod_sound = rfmod_sys.create_sound (
    "ping.wav",
    Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
    Some (&mut default_soundex_info)
  ).unwrap();
  // print sound infos
  //fmod_utils::write_info_rfmod_sound (&mut std::io::stdout(), &rfmod_sound).unwrap();
  println!("...rfmod wav sound created...");

  // new midi sound
  let mut midi_soundex_info  = rfmod::CreateSoundexInfo::default();
  //midi_soundex_info.dls_name = "gm.dls".to_string();
  midi_soundex_info.dls_name = "OPL-3_FM_128M_22050s.dls".to_string();
  let rfmod_sound_midi       = rfmod_sys.create_sound (
    "988-v01.mid",
    Some (rfmod::Mode (rfmod::DEFAULT)),
    Some (&mut midi_soundex_info)
  ).unwrap();
  println!("...rfmod midi sound created...");

  // new dsp echo
  let rfmod_dsp_echo = rfmod_sys.create_DSP_by_type (rfmod::DspType::Echo)
    .unwrap();
  rfmod_ok!(
    rfmod_dsp_echo.set_parameter (rfmod::DspTypeEcho::Delay as i32, 500.0));
  rfmod_ok!(
    rfmod_dsp_echo.set_parameter (rfmod::DspTypeEcho::DecayRatio as i32, 0.8));
  rfmod_ok!(
    rfmod_dsp_echo.set_parameter (rfmod::DspTypeEcho::WetMix as i32, 0.3));
  //fmod_utils::write_info_rfmod_dsp (&mut std::io::stdout(), &rfmod_dsp_echo).unwrap();
  let _dsp_connection = rfmod_sys.add_DSP (&rfmod_dsp_echo).unwrap();
  println!("...rfmod echo dsp created...");

  rfmod_ok!(rfmod_sys.update());

  // readline loop
  println!("readline loop...");
  println!("commands:\n    \
    'play' -- play the sample ping wav\n    \
    'demo' -- demo that pans a repeating ping sample from left to right\n    \
    'midi' -- play the sample midi file\n    \
    'quit' -- quit");
  'readline_loop: loop {
    print!(" > ");
    use std::io::Write;
    let _     = std::io::stdout().flush();
    let mut s = String::new();
    let _     = std::io::stdin().read_line (&mut s);
    if !s.trim_end().is_empty() {
      let word_ct     = s.as_str().split_whitespace().count();
      let mut words   = s.as_str().split_whitespace();
      #[allow(unused_assignments)]
      let mut handled = true;
      match word_ct {
        0 => unreachable!("ERROR: zero words in server input readline parse"),
        1 => {
          let command = words.next().unwrap();
          use std::str::pattern::Pattern;
          if command.is_prefix_of("quit") {
            // quit
            break 'readline_loop;
          } else if command.is_prefix_of("play") {
            // play wav sound
            let _rfmod_channel = rfmod_sound.play().unwrap();
          } else if command.is_prefix_of("midi") {
            // play midi sound
            let _rfmod_channel = rfmod_sound_midi.play().unwrap();
          } else if command.is_prefix_of("demo") {
            // demo
            let mut position = rfmod::Vector { x: 0.0, y: 0.0, z: -2.0 };
            let velocity     = rfmod::Vector { x: 20.0, y: 0.0, z: 0.0 };
            for i in 0..101 {
              let x = -100.0 + 2.0*(i as f32);
              position.x = x;
              let rfmod_channel = rfmod_sound.play().unwrap();
              rfmod_ok!(rfmod_channel.set_3D_attributes (&position, &velocity));
              // sys update
              rfmod_ok!(rfmod_sys.update());
              std::thread::sleep (std::time::Duration::from_millis (100));
            }
          } else {
            handled = false;
          }
        },
        _ => handled = false
      } // end match word count
      if !handled {
        println!("unrecognized command: \"{}\"", s.trim());
      }
    } // end input not empty
  } // end 'readline_loop
  println!("...readline loop");

  println!("...fmod-utils example main");
}
