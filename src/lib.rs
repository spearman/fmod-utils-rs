extern crate rfmod;

/// Assert that expression yields `rfmod::Status::Ok`
#[macro_export]
macro_rules! rfmod_ok {
  ($e:expr) => {
    assert_eq!($e, rfmod::Status::Ok)
  }
}

//
//  fn write_info_rfmod_sys
//
pub fn write_info_rfmod_sys <W : std::io::Write>
  (writer : &mut W, sys : &rfmod::Sys) -> Result <(), std::io::Error>
{
  try!{ writeln!(writer, "# rfmod sys info #") }

  let fmod_version = sys.get_version().unwrap();
  try!{ writeln!(writer,
    "rfmod sys: version:              {:x}.{:x}.{:x}",
      (fmod_version & 0xffff0000) >> 16,
      (fmod_version & 0x0000ff00) >> 8,
      (fmod_version & 0x000000ff))
  }
  try!{ writeln!(writer,
    "rfmod sys: output:               {:?}", sys.get_output().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: num drivers:          {}", sys.get_num_drivers().unwrap())
  }
  for i in 0..sys.get_num_drivers().unwrap() {
    let (guid, name) = sys.get_driver_info (i, 256).unwrap();
    try!{ writeln!(writer,
      "rfmod sys: driver info[{}]:\n  \
        {}\n  \
        Guid {{ {:x}, {:x}, {:x}, {:?} }}",
      i, name, guid.data1, guid.data2, guid.data3, guid.data4)
    }
    let driver_caps = sys.get_driver_caps (i);
    match driver_caps {
      Ok ((fmod_caps, n, speaker_mode)) => {
        try!{ writeln!(writer,
          "rfmod sys: driver caps[{}]:     Ok (FmodCaps ({}), {}, {:?})",
          i, fmod_caps.0, n, speaker_mode)
        }
      }
      Err (err) => {
        try!{ writeln!(writer,
          "rfmod sys: driver caps[{}]:     Err ({:?})", i, err)
        }
      }
    }
  }

  try!{ writeln!(writer,
    "rfmod sys: record num drivers:   {}",
      sys.get_record_num_drivers().unwrap())
  }
  for i in 0..sys.get_record_num_drivers().unwrap() {
    let (guid, name) = sys.get_record_driver_info (i, 256).unwrap();
    try!{ writeln!(writer,
      "rfmod sys: record driver info[{}]:\n  \
        {}\n  \
        Guid {{ {:x}, {:x}, {:x}, {:?} }}",
      i, name, guid.data1, guid.data2, guid.data3, guid.data4)
    }
    let driver_caps = sys.get_record_driver_caps (i);
    match driver_caps {
      Ok ((fmod_caps, n, speaker_mode)) => {
        try!{ writeln!(writer,
          "rfmod sys: record driver caps[{}]: Ok (FmodCaps ({}), {}, {:?})",
          i, fmod_caps.0, n, speaker_mode)
        }
      }
      Err (err) => {
        try!{ writeln!(writer,
          "rfmod sys: record driver caps[{}]: Err ({:?})", i, err)
        }
      }
    }
  }

  try!{ writeln!(writer,
    "rfmod sys: driver:               {}", sys.get_driver().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: hardware channels:    {}",
      sys.get_hardware_channels().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: software channels:    {}",
      sys.get_software_channels().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: channels playing:     {}",
      sys.get_channels_playing().unwrap())
  }
  let software_format = sys.get_software_format().unwrap();
  try!{ writeln!(writer,
    "rfmod sys: software format:\n  \
      sample rate:          {}\n  \
      format:               {:?}\n  \
      num output channels:  {}\n  \
      max input channels:   {}\n  \
      resample method:      {:?}\n  \
      bits:                 {}",
      software_format.sample_rate,
      software_format.format,
      software_format.num_output_channels,
      software_format.max_input_channels,
      software_format.resample_method,
      software_format.bits)
  }
  try!{ writeln!(writer,
    "rfmod sys: dsp buffer size:      {:?}",
      sys.get_DSP_buffer_size().unwrap())
  }

  // TODO: get_advanced_settings

  try!{ writeln!(writer,
    "rfmod sys: speaker mode:         {:?}",
      sys.get_speaker_mode().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: num plugins:\n  \
      PluginType::Output:   {:?}\n  \
      PluginType::Codec:    {:?}\n  \
      PluginType::DSP:      {:?}",
      sys.get_num_plugins (rfmod::PluginType::Output).unwrap(),
      sys.get_num_plugins (rfmod::PluginType::Codec).unwrap(),
      sys.get_num_plugins (rfmod::PluginType::DSP).unwrap())
  }

  // TODO: get_plugin_handle, get_plugin_info

  try!{ writeln!(writer,
    "rfmod sys: output by plugin:     PluginHandle ({})",
      sys.get_output_by_plugin().unwrap().0)
  }
  try!{ writeln!(writer,
    "rfmod sys: 3d num listeners:     {}",
      sys.get_3D_num_listeners().unwrap())
  }

  for i in 0..sys.get_3D_num_listeners().unwrap() {
    let (pos, vel, forward, up) = sys.get_3D_listener_attributes (i)
      .unwrap();
    try!{ writeln!(writer,
      "rfmod sys: 3d listener attributes[{}]:\n  \
        pos:      {:?}\n  \
        vel:      {:?}\n  \
        forward:  {:?}\n  \
        up:       {:?}",
        i, pos, vel, forward, up)
    }
  }

  try!{ writeln!(writer,
    "rfmod sys: 3d speaker position:\n  \
      Speaker::FrontLeft:    {:?}\n  \
      Speaker::FrontRight:   {:?}\n  \
      Speaker::FrontCenter:  {:?}\n  \
      Speaker::LowFrequency: {:?}\n  \
      Speaker::BackLeft:     {:?}\n  \
      Speaker::BackRight:    {:?}\n  \
      Speaker::SideLeft:     {:?}\n  \
      Speaker::SideRightt:   {:?}",
      sys.get_3D_speaker_position (rfmod::Speaker::FrontLeft).unwrap(),
      sys.get_3D_speaker_position (rfmod::Speaker::FrontRight).unwrap(),
      sys.get_3D_speaker_position (rfmod::Speaker::FrontCenter).unwrap(),
      sys.get_3D_speaker_position (rfmod::Speaker::LowFrequency).unwrap(),
      sys.get_3D_speaker_position (rfmod::Speaker::BackLeft).unwrap(),
      sys.get_3D_speaker_position (rfmod::Speaker::BackRight).unwrap(),
      sys.get_3D_speaker_position (rfmod::Speaker::SideLeft).unwrap(),
      sys.get_3D_speaker_position (rfmod::Speaker::SideRight).unwrap())
  }
  let (doppler_scale, distance_factor, roll_off_scale)
    = sys.get_3D_settings().unwrap();
  try!{ writeln!(writer,
    "rfmod sys: 3d settings:\n  \
      doppler scale:   {}\n  \
      distance factor: {}\n  \
      roll off scale:  {}",
      doppler_scale, distance_factor, roll_off_scale)
  }
  let (buffer_size, time_unit) = sys.get_stream_buffer_size().unwrap();
  try!{ writeln!(writer,
    "rfmod sys: stream buffer size:   ({}, TimeUnit ({}))",
      buffer_size, time_unit.0)
  }
  try!{ writeln!(writer,
    "rfmod sys: cpu usage:            {:?}", sys.get_CPU_usage().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: sound ram:            {:?}", sys.get_sound_RAM().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: num cdrom drives:     {:?}",
      sys.get_num_CDROM_drives().unwrap())
  }

  // TODO: get_CDROM_drive_name

  // TODO: get_master_channel_group

  // TODO: get_master_sound_group

  try!{ write!(writer,
    "rfmod sys: properties: {}",
      show_rfmod_reverb_properties (&sys.get_reverb_properties().unwrap()))
  }

  // TODO: get_reverb_ambient_properties

  // TODO: get_DSP_head

  try!{ writeln!(writer,
    "rfmod sys: dsp clock:            {:?}", sys.get_DSP_clock().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: geometry settings:    {}",
      sys.get_geometry_settings().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sys: geometry occlusion:   {:?}",
      sys.get_geometry_occlusion().unwrap())
  }

  Ok(())
} // end fn write_info_rfmod_sys

//
//  fn write_info_rfmod_reverb
//
pub fn write_info_rfmod_reverb <W : std::io::Write>
  (writer : &mut W, reverb : &rfmod::Reverb) -> Result <(), std::io::Error>
{
  try!{ writeln!(writer, "# rfmod reverb info #") }

  try!{ writeln!(writer,
    "rfmod reverb: active: {}", reverb.get_active().unwrap())
  }

  let (position, min_distance, max_distance)
    = reverb.get_3D_attributes().unwrap();
  try!{ writeln!(writer,
    "rfmod reverb: 3d attributes:\n    \
      position:     {:?}\n    \
      min_distance: {}\n    \
      max_distance: {}",
      position, min_distance, max_distance)
  }

  try!{ write!(writer, "rfmod reverb: properties: {}",
    show_rfmod_reverb_properties (
      &reverb.get_properties (rfmod::ReverbProperties::default()).unwrap()
    ))
  }

  Ok(())
} // end fn write_info_rfmod_reverb

//
//  fn write_info_rfmod_dsp
//
pub fn write_info_rfmod_dsp <W : std::io::Write>
  (writer : &mut W, dsp : &rfmod::Dsp) -> Result <(), std::io::Error>
{
  try!{ writeln!(writer, "# rfmod dsp info #") }

  try!{ writeln!(writer,
    "rfmod dsp: type:                 {:?}", dsp.get_type().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod dsp: active:               {}", dsp.get_active().unwrap())
  }
  // TODO: active speakers ?
  try!{ writeln!(writer,
    "rfmod dsp: bypass:               {}", dsp.get_bypass().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod dsp: num inputs:           {}", dsp.get_num_inputs().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod dsp: num outputs:          {}", dsp.get_num_outputs().unwrap())
  }
  // TODO: show inputs/outputs ?
  // TODO: parameters ?
  // TODO: memory info ?

  Ok(())
} // end fn write_info_rfmod_dsp

//
//  fn write_info_rfmod_channel
//
/// TODO: this is probably unsafe to be called on a non-3D channel as most
/// results are unwrapped
pub fn write_info_rfmod_channel <W : std::io::Write> (
  writer        : &mut W,
  channel : &rfmod::Channel
) -> Result <(), std::io::Error> {
  try!{ writeln!(writer, "# rfmod channel info #") }

  let is_init = channel.is_init();
  try!{ writeln!(writer,
    "rfmod channel: is init:              {}", is_init)
  }

  if !is_init {
    return Ok (())
  }

  try!{ writeln!(writer,
    "rfmod channel: index:                {}", channel.get_index().unwrap())
  }

  // FIXME: the following fails for a newly created channel,
  // how to check that these will not fail?

  try!{ writeln!(writer,
    "rfmod channel: mode:                 Mode ({:b})",
      channel.get_mode().unwrap().0)
  }
  try!{ writeln!(writer,
    "rfmod channel: priority:             {}", channel.get_priority().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: is virtual:           {}", channel.is_virtual().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: is playing:           {}", channel.is_playing().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: position (ms):        {}",
      channel.get_position (rfmod::TIMEUNIT_MS).unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: paused:               {}", channel.get_paused().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: loop count:           {}", channel.get_loop_count().unwrap())
  }
  let (loop_start, loop_end)
    = channel.get_loop_points (rfmod::TIMEUNIT_MS, rfmod::TIMEUNIT_MS)
        .unwrap();
  try!{ writeln!(writer,
    "rfmod channel: loop points (ms):     (start: {}, end: {})",
      loop_start, loop_end)
  }
  try!{ writeln!(writer,
    "rfmod channel: volume:               {}", channel.get_volume().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: mute:                 {}", channel.get_mute().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: audibility:           {}", channel.get_audibility().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: frequency:            {}", channel.get_frequency().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: low pass gain:        {}",
      channel.get_low_pass_gain().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: speaker mix options: {:#?}",
      channel.get_speaker_mix().unwrap())
  }
  // TODO: speaker levels ?
  // TODO: input channel mixes ?

  let delay_type_end_ms
    = channel.get_delay (rfmod::DelayType::EndMS).unwrap();
  let delay_type_dsp_clock_start
    = channel.get_delay (rfmod::DelayType::DSPClockStart).unwrap();
  let delay_type_dsp_clock_end
    = channel.get_delay (rfmod::DelayType::DSPClockEnd).unwrap();
  let delay_type_dsp_clock_pause
    = channel.get_delay (rfmod::DelayType::DSPClockPause).unwrap();
  try!{ writeln!(writer, "rfmod channel: delays:") }
  // TODO: automatic alignment/formatting in the following ?
  try!{ writeln!(writer, "    \
    (DelayType::{:?},         delay_hi: {}, delay_lo: {})\n    \
    (DelayType::{:?}, delay_hi: {}, delay_lo: {})\n    \
    (DelayType::{:?},   delay_hi: {}, delay_lo: {})\n    \
    (DelayType::{:?}, delay_hi: {}, delay_lo: {})",
    delay_type_end_ms.0, delay_type_end_ms.1, delay_type_end_ms.2,
    delay_type_dsp_clock_start.0, delay_type_dsp_clock_start.1,
      delay_type_dsp_clock_start.2,
    delay_type_dsp_clock_end.0, delay_type_dsp_clock_end.1,
      delay_type_dsp_clock_end.2,
    delay_type_dsp_clock_pause.0, delay_type_dsp_clock_pause.1,
      delay_type_dsp_clock_pause.2)
  }

  let reverb_channel_properties = channel.get_reverb_properties().unwrap();
  // TODO: show Dsp contents of field connection_point ?
  try!{ writeln!(writer,
    "rfmod channel: reverb channel properties: ReverbChannelProperties {{\n    \
      direct:           {}\n    \
      room:             {}\n    \
      flags:            {}\n    \
      connection point: Dsp {{ ... }}\n\
    }}",
      reverb_channel_properties.direct,
      reverb_channel_properties.room,
      reverb_channel_properties.flags)
  }

  // TODO: show channel group ?
  // TODO: dsp head ?
  // TODO: memory info ?

  // FIXME: the following fails for a 3D channel,
  // how to check if channel is 2D or 3D ?
  /*
  try!{ writeln!(writer,
    "rfmod channel: pan:              {}", channel.get_pan().unwrap())
  }
  */

  let channel_3d_attributes = channel.get_3D_attributes().unwrap();
  try!{ writeln!(writer,
    "rfmod channel: 3d attributes:\n    \
      position: {:?}\n    \
      velocity: {:?}",
      channel_3d_attributes.0, channel_3d_attributes.1)
  }
  try!{ writeln!(writer,
    "rfmod channel: 3d min max distance:  {:?}",
      channel.get_3D_min_max_distance().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: 3d spread:            {}", channel.get_3D_spread().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: 3d pan level:         {}",
      channel.get_3D_pan_level().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: 3d doppler level:     {}",
      channel.get_3D_doppler_level().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: 3d custom rolloff:    {:?}",
      channel.get_3D_custom_rolloff().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: 3d distance filter:   {:?}",
      channel.get_3D_distance_filter().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: 3d occlusion:  {:?}", channel.get_3D_occlusion().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel: 3d cone orientation:  {:?}",
      channel.get_3D_cone_orientation().unwrap())
  }
  let (inside_cone_angle, outside_cone_angle, outside_volume)
    = channel.get_3D_cone_settings().unwrap();
  try!{ writeln!(writer,
    "rfmod channel: 3d cone settings:\n    \
      inside cone angle:  {}\n    \
      outside cone angle: {}\n    \
      outside volume:     {}",
      inside_cone_angle, outside_cone_angle, outside_volume)
  }

  Ok(())
} // end fn write_info_rfmod_channel

//
//  fn write_info_rfmod_channel_group
//
pub fn write_info_rfmod_channel_group <W : std::io::Write> (
  writer              : &mut W,
  channel_group : &rfmod::ChannelGroup
) -> Result <(), std::io::Error> {
  try!{ writeln!(writer, "# rfmod channel group info #") }

  try!{ writeln!(writer,
    "rfmod channel group: name:         {}",
      channel_group.get_name (256).unwrap())
  }
  try!{ writeln!(writer,
    "rfmod channel group: num channels: {}",
      channel_group.get_num_channels().unwrap())
  }

  // TODO: more

  Ok(())
} // end fn write_info_rfmod_channel_group

//
//  fn write_info_rfmod_sound
//
pub fn write_info_rfmod_sound <W : std::io::Write> (
  writer  : &mut W,
  sound   : &rfmod::Sound
) -> Result <(), std::io::Error> {
  try!{ writeln!(writer, "# rfmod sound info #") }

  try!{ writeln!(writer,
    "rfmod sound: name:             {}", sound.get_name (256).unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sound: length (ms):      {}",
      sound.get_length (rfmod::TIMEUNIT_MS).unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sound: format:           {:?}", sound.get_format().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sound: num sub sounds:   {}", sound.get_num_sub_sounds().unwrap())
  }
  // TODO: the following returns Err (Format):
  /*
  try!{ writeln!(writer,
    "rfmod sound: num channels:     {}", sound.get_num_channels().unwrap())
  }
  */
  // TODO: sub sounds
  try!{ writeln!(writer,
    "rfmod sound: num tags:         {:?}", sound.get_num_tags().unwrap())
  }
  // TODO: tags
  try!{ writeln!(writer,
    "rfmod sound: open state:       {:?}", sound.get_open_state().unwrap())
  }
  try!{ writeln!(writer,
    "rfmod sound: mode:             Mode ({:b})", sound.get_mode().unwrap().0)
  }
  try!{ writeln!(writer,
    "rfmod sound: loop count:       {}", sound.get_loop_count().unwrap())
  }
  let (loop_start, loop_end)
    = sound.get_loop_points (rfmod::TIMEUNIT_MS, rfmod::TIMEUNIT_MS)
        .unwrap();
  try!{ writeln!(writer,
    "rfmod sound: loop points (ms): (start: {}, end: {})",
      loop_start, loop_end)
  }
  // TODO: the following returns Err (Format):
  /*
  try!{ writeln!(writer,
    "rfmod sound: music speed:      {}", sound.get_music_speed().unwrap())
  }
  */
  let (frequency, volume, pan, priority) = sound.get_defaults().unwrap();
  try!{ writeln!(writer,
    "rfmod sound: defaults:\n  \
      frequency:  {}\n  \
      volume:     {}\n  \
      pan:        {}\n  \
      priority:   {}",
      frequency, volume, pan, priority)
  }
  let (frequency_var, volume_var, pan_var) = sound.get_variations().unwrap();
  try!{ writeln!(writer,
    "rfmod sound: variations:\n  \
      frequency var:  {}\n  \
      volume var:     {}\n  \
      pan var:        {}",
      frequency_var, volume_var, pan_var)
  }
  try!{ writeln!(writer,
    "rfmod sound: num sync points:  {}", sound.get_num_sync_points().unwrap())
  }
  // TODO: sync point info
  try!{ writeln!(writer,
    "rfmod sound: 3d min max distance: {:?}",
      sound.get_3D_min_max_distance().unwrap())
  }
  let (inside_cone_angle, outside_cone_angle, outside_volume)
    = sound.get_3D_cone_settings().unwrap();
  try!{ writeln!(writer,
    "rfmod sound: 3d cone settings:\n  \
      inside cone angle:  {}\n  \
      outside cone angle: {}\n  \
      outside volume:     {}",
      inside_cone_angle, outside_cone_angle, outside_volume)
  }

  // TODO: 3d custom rolloffs ?

  // TODO: memory info

  Ok(())
} // end fn write_info_rfmod_sound

//
//  fn show_rfmod_reverb_properties
//
pub fn show_rfmod_reverb_properties (
  reverb_properties : &rfmod::ReverbProperties
) -> String {
  use std::fmt::Write;

  let mut s = String::new();
  writeln!(s, "ReverbProperties {{").unwrap();
  writeln!(s,
    "    instance:          {}", reverb_properties.instance).unwrap();
  writeln!(s,
    "    environment:       {}", reverb_properties.environment).unwrap();
  writeln!(s,
    "    env diffusion:     {}", reverb_properties.env_diffusion).unwrap();
  writeln!(s,
    "    room:              {}", reverb_properties.room).unwrap();
  writeln!(s,
    "    room hf:           {}", reverb_properties.room_HF).unwrap();
  writeln!(s,
    "    room lf:           {}", reverb_properties.room_LF).unwrap();
  writeln!(s,
    "    decay time:        {}", reverb_properties.decay_time).unwrap();
  writeln!(s,
    "    decay hf ratio:    {}", reverb_properties.decay_HF_ratio).unwrap();
  writeln!(s,
    "    decay lf ratio:    {}", reverb_properties.decay_LF_ratio).unwrap();
  writeln!(s,
    "    reflections:       {}", reverb_properties.reflections).unwrap();
  writeln!(s,
    "    reflections delay: {}", reverb_properties.reflections_delay).unwrap();
  writeln!(s,
    "    reverb:            {}", reverb_properties.reverb).unwrap();
  writeln!(s,
    "    reverb delay:      {}", reverb_properties.reverb_delay).unwrap();
  writeln!(s,
    "    modulation time:   {}", reverb_properties.modulation_time).unwrap();
  writeln!(s,
    "    modulation depth:  {}", reverb_properties.modulation_depth).unwrap();
  writeln!(s,
    "    hf reference:      {}", reverb_properties.HF_reference).unwrap();
  writeln!(s,
    "    lf reference:      {}", reverb_properties.LF_reference).unwrap();
  writeln!(s,
    "    diffustion:        {}", reverb_properties.diffusion).unwrap();
  writeln!(s,
    "    density:           {}", reverb_properties.density).unwrap();
  writeln!(s,
    "    flags:             {:b}", reverb_properties.flags).unwrap();
  writeln!(s, "}}").unwrap();

  s
} // end fn show_rfmod_reverb_properties

/*
#[cfg(test)]
mod tests {
  #[test]
  fn it_works() {
      assert_eq!(2 + 2, 4);
  }
}
*/
